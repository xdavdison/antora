= Version Key

A version is defined by assigning a value to the `version` key.
It's important to understand <<usage,how Antora uses the version>> and its related facets before committing to a versioning scheme.

== What's a version?

In Antora, a [.term]*version* is the value assigned to the `version` key in a component version descriptor file ([.path]_antora.yml_) or on a content source in the playbook.
A version is a semantic or named identifier that often represents a unique or final release of the documentation associated with a project.
A version can also be defined as xref:component-with-no-version.adoc[unversioned] by assigning a tilde, `~`, to the `version` key.
The value of a `version` key, in combination with the value of a `name` key, defines a xref:component-version.adoc[component version].

Occasionally, the version is called the [.term]*actual version* when a distinction between version and the other version facets--xref:version-facets.adoc#prerelease[prerelease], xref:version-facets.adoc#display[display], and xref:version-facets.adoc#symbolic[symbolic]&mdash;needs to be made in a description or example.

[#usage]
=== How Antora uses version

The version is fundamental to many of Antora's operations.
Antora uses the version:

* to xref:how-component-versions-are-sorted.adoc[sort component versions]
* identify the xref:how-component-versions-are-sorted.adoc#latest-version[latest version of a component]
* apply routing rules
* as the xref:how-antora-builds-urls.adoc#version[version segment] in page and asset URLs, except when:
** the assigned value is the xref:component-with-no-version.adoc[tilde, ~, that defines an unversioned component version]
** a component version is identified as the latest stable or latest prerelease version and the xref:playbook:urls-latest-version-segment.adoc[latest_version_segment key] or xref:playbook:urls-latest-prerelease-version-segment.adoc[latest_prerelease_version_segment key] is set in the playbook
* for display purposes in the reference UI xref:navigation:index.adoc#component-dropdown[component version selector] and xref:navigation:index.adoc#page-dropdown[page version selector] menus, except when:
** the `display_version` key is set and assigned a value in the component version's [.path]_antora.yml_ file
** the `display_version` key is set by Antora at runtime because the xref:component-prerelease.adoc#identifier[prerelease key is assigned an identifier]

Content writers use the version as a coordinate in page and resource IDs when referencing a resource in another component version.

[#key]
== version key

The `version` key is set and assigned a value in a component version descriptor file ([.path]_antora.yml_).
The `version` key accepts a named identifier, such as `jesse`, a semantic identifier, such as `1.5`, or the reserved tilde (`~`), that defines a component version as unversioned.
To learn about specify an unversioned component version, see xref:component-with-no-version.adoc[].

The following examples describe how to assign a named or semantic identifier to `version`.

.antora.yml with named identifier assigned to version
[source#ex-name,yaml]
----
name: star
version: rigel # <.>
----
<.> On a new line, type `version`, directly followed by a colon and a space (`++: ++`).
Then type the value you want assigned to `version`.

Semantic identifiers begin with a number.
Antora allows the identifier to be prefixed with the letter _v_, which it ignores.
Enclose values that start with a number, like the one shown in <<ex-semver>>, in a set of single quote marks (`'`).

.antora.yml with semantic identifier assigned to version
[source#ex-semver,yaml]
----
name: colorado
version: '5.6' # <.>
----
<.> Enclose values that start with a number in a set of single quote marks (`'`).

Antora recognizes semantic identifiers according to the https://semver.org[semantic versioning rules].
A semantic identifier is an integer, begins with a number and contains at least one dot (`.`), or begins with `v`, followed by a number, and contains at least one dot (`.`).
`3`, `v9.0`, and `5.1` are examples of semantic identifiers.

//If a component version is a prerelease, set the xref:component-prerelease.adoc[prerelease key] in addition to `version`.

[#requirements]
== version requirements

The value assigned to the `version` key can contain letters, numbers, periods (`.`), underscores (`+_+`), and hyphens (`-`).
To ensure portability between host platforms, letters used in the `version` value should be lowercase.

The value *cannot* contain spaces, forward slashes (`/`), or HTML special characters (`&`, `<`, or `>`).
See xref:component-display-version.adoc[] to learn how to display an identifier that contains spaces, uppercase letters, and other characters in the UI menus.
